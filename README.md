<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Projeto para o teste de estagiário da DIX_BPO

- Nome: Alexandre Souza dos Santos
- idade: 21 Anos
- Curso: Sistemas de Informação

## Instalação do projeto

- clone o projeto
- composer install
- crie e configure o .env com as informações do banco de dados
- php artisan key:generate
- php artisan migrate --seed 

## Informações de acesso 

- Os dados de acesso estão na tela de login
 
## Informações do projeto

- O projeto tem uma tela inicial com um contador de usuários e noticias
- As notícias podem ser criadas no modo privado onde só quem postou pode acessar ou no modo publico onde todos podem ver
- Aba de gerenciamento de usuários


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
