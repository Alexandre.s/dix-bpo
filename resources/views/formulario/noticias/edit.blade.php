@extends('layouts.app', ['pageSlug' => 'dashboard'])

@section('content')
<form name="formNoticia" id="formNoticia" method="post" action="{{route('noticias.update', $posts->id)}}"  class="container" >
  @csrf
  @include('alerts.success')

  <div class="form-group">
    <label for="exampleInputEmail1">Titulo da notícia</label>
    <input type="text" class="form-control" id="title" name="title" aria-describedby="emailHelp" placeholder="Notícia vira assunto da semana" value="{{$posts->title}}">
  </div>
  <div class="form-group">
    <label for="exampleFormControlTextarea1">Conteúdo da notícia</label>
    <input type="textarea" class="form-control" id="title" name="body" aria-describedby="emailHelp" placeholder="Notícia vira assunto da semana" value="{{$posts->body}}">
  </div>
  <div class="form-group">
    <label for="exampleFormControlSelect1">Status da postagem</label>
    <select class="form-control" id="exampleFormControlSelect1" name="status" >
      <option value="{{$posts->status}}">Selecione</option>
      <option value="public">Publico</option>
      <option value="private">Privado</option>
    </select>
  </div>
  <button type="submit" class="btn btn-primary">Atualizar</button>
</form>
@endsection

@push('js')
    <script src="{{ asset('white') }}/js/plugins/chartjs.min.js"></script>
    <script>
        $(document).ready(function() {
          demo.initDashboardPageCharts();
        });
    </script>
@endpush

