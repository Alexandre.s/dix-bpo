@extends('layouts.app', ['pageSlug' => 'dashboard'])

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading">
                    <h1>
                        {{ $posts->title }} 
                        <a href="{{ route('home') }}" class="btn btn-success btn-default pull-right">Voltar</a>
                    </h1>
                </div>
                <div class="panel-body">
                    <div class="mb-4" style="margin-top: 10px;">
                        </b>{{ $posts->body }}</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@push('js')
    <script src="{{ asset('white') }}/js/plugins/chartjs.min.js"></script>
    <script>
        $(document).ready(function() {
          demo.initDashboardPageCharts();
        });
    </script>
@endpush

