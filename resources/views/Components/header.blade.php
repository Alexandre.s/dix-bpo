
<form class="form-inline my-2 my-lg-0" method="post" action="{{route('noticias.search')}}">
  @csrf
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" name='search'>
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>

        <div class="card-body all-icons">
          <div class="row">
   
            @forelse ($posts as $post)
           

  <div class="font-icon-list col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xs-6">
              <div class="font-icon-detail">
                <h4 class="item-menu"> 
               <p class="titulo-item">titulo</p> {{ \Illuminate\Support\Str::limit($post->title ?? '',30,' ...') }}
                </h4>
                <h6>Autor: <b>{{$post->author}}</b></h6>
                <h6>status: <b>{{$post->status}}</b></h6>

                <a href="{{ route('noticias.show', $post->id) }}">
                <i class=" tim-icons icon-zoom-split"></i>
                </a>
                <a href="{{ route('noticias.edit', $post->id) }}">
                <i class=" tim-icons icon-pencil"></i>
                </a>
                <a href="{{ route('noticias.delete', $post->id) }}">
                <i class=" tim-icons icon-trash-simple"></i>
                </a>
              </div>
            </div>
        @empty
        <h1 class="ml-4"> Nenhuma notícia cadastrada</h1>
@endforelse



        </div>

        </div>
    
  