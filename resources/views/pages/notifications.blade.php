@extends('layouts.app', ['pageSlug' => 'dashboard'])

@section('content')
<div class="row">
        <div class="col-12">
            <div class="card card-chart">
                <div class="card-header ">
                    <div class="row">
                        <div class="col-sm-6 text-left">
                            <h5 class="card-category">Novas postagens</h5>
                            <h2 class="card-title">Notícias</h2>
                        </div>


                        <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4 id="title">Bem vindo ao painel <span id="text-name-dashbiard">
          {{ Auth::user()->name }}
          </span> 
  
</h4>
          <p class="category">Seus clientes estão ansiosos para ver novas notícias
          </p>
        </div>
            <div class="row">
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header" id='card-btn'>
                <div class="row">
                    <div class="col-8">
                        <h4 class="card-title-notice">Minhas notícias</h4>
                    </div>
                    <div class="col-4 text-right">
                        <a href="{{ route('noticias.form') }}" class="btn btn-md " id="btn-adc">Adicionar</a>
                    </div>
                </div>
            </div>
            </div>
            </div>
            </div>


</div>
@include('Components.header')
@endsection

@push('js')
    <script src="{{ asset('white') }}/js/plugins/chartjs.min.js"></script>
    <script>
        $(document).ready(function() {
          demo.initDashboardPageCharts();
        });
    </script>
@endpush
