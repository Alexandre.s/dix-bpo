@extends('layouts.app', ['pageSlug' => 'dashboard'])

@section('content')
<div class="row">
        <div class="col-12">
            <div class="card card-chart">
                <div class="card-header ">
                    <div class="row">
                        <div class="col-sm-6 text-left">
                            <h5 class="card-category">Novas postagens</h5>
                            <h2 class="card-title">Notícias</h2>
                        </div>


                        <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4 id="title">Bem vindo ao painel <span id="text-name-dashbiard">
          {{ Auth::user()->name }}
          </span> 
  
</h4>
          <p class="category">Seus clientes estão ansiosos para ver novas notícias
          </p>
        </div>
            </div>

        <div class="card-body all-icons">
          <div class="row">
  <div class="font-icon-list col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xs-6">
              <div class="font-icon-detail">
                <h3 class="item-menu mt-2"> 
                 Usuários
                </h3>
               <h1>{{$users}}</h1>

              </div>
            </div>

  <div class="font-icon-list col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xs-6">
              <div class="font-icon-detail">
                <h3 class="item-menu mt-2"> 
                 Notícias
                </h3>
               <h1>{{$noticias}}</h1>

              </div>
            </div>

  

        </div>
        </div>

  






@endsection

@push('js')
    <script src="{{ asset('white') }}/js/plugins/chartjs.min.js"></script>
    <script>
        $(document).ready(function() {
          demo.initDashboardPageCharts();
        });
    </script>
@endpush
