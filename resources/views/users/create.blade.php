@extends('layouts.app', ['pageSlug' => 'dashboard'])

@section('content')
<form name="formUsers" id="formuIsers" method="post" action="{{route('users.create')}}" class="container">
  @csrf

  @include('alerts.success')

  <div class="form-group">
    <label for="exampleInputEmail1">Nome</label>
    <input type="text" class="form-control" id="name" name="name" aria-describedby="emailHelp" placeholder="Ex: Alexandre Souza" required>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">email</label>
    <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Ex: desenvoldvedor@dixbpo.com" required>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Senha</label>
    <input type="password" class="form-control" id="password" name="password" aria-describedby="emailHelp" placeholder="******" required>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Confirmar senha</label>
    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" aria-describedby="emailHelp" placeholder="******" required>
  </div>

  <button type="submit" class="btn btn-primary mt-4">Criaar usuário</button>
</form>
@endsection

@push('js')
    <script src="{{ asset('white') }}/js/plugins/chartjs.min.js"></script>
    <script>
        $(document).ready(function() {
          demo.initDashboardPageCharts();
        });
    </script>
@endpush

