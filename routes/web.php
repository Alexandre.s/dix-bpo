<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Auth::routes();

Route::get('/home', 'App\Http\Controllers\HomeController@usersCount')->name('home')->middleware('auth');
Route::get('/noticias/form', 'App\Http\Controllers\admin\NoticiasController@index')->name('noticias.form')->middleware('auth');
Route::post('/noticias/create', 'App\Http\Controllers\admin\NoticiasController@store')->name('noticias.create')->middleware('auth');
Route::get('/noticias/show/{id}', 'App\Http\Controllers\admin\NoticiasController@showNoticias')->name('noticias.show')->middleware('auth');
Route::get('/noticias/edit/{id}', 'App\Http\Controllers\admin\NoticiasController@edit')->name('noticias.edit')->middleware('auth');
Route::get('/noticias/delete/{id}', 'App\Http\Controllers\admin\NoticiasController@destroy')->name('noticias.delete')->middleware('auth');
Route::post('/noticias/update/{id}', 'App\Http\Controllers\admin\NoticiasController@update')->name('noticias.update')->middleware('auth');
Route::post('/noticias/search', 'App\Http\Controllers\admin\NoticiasController@search')->name('noticias.search')->middleware('auth');
Route::get('/users/formcreate', 'App\Http\Controllers\UserController@formcreate')->name('users.formcreate')->middleware('auth');
Route::post('/users/create', 'App\Http\Controllers\UserController@create')->name('users.create')->middleware('auth');
Route::get('/users/edit/{id}', 'App\Http\Controllers\UserController@edit')->name('users.edit')->middleware('auth');
Route::put('/users/update/{id}', 'App\Http\Controllers\UserController@update')->name('users.update')->middleware('auth');
Route::get('/users/delete/{id}', 'App\Http\Controllers\UserController@destroy')->name('users.delete')->middleware('auth');








Route::group(['middleware' => 'auth'], function () {
		Route::get('maps', ['as' => 'pages.maps', 'uses' => 'App\Http\Controllers\PageController@maps']);
		Route::get('notifications', ['as' => 'pages.notifications', 'uses' => 'App\Http\Controllers\PageController@notifications']);
});

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
});

