<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Noticia extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'body',
        'author',
        'status',
        'xreated_at',
        'updated_at',

    ];
    public function public($query)
    {
        $data = $query->where('status', 'public');
        dd($data);
    }
    public function private($query)
    {
        $data = $query->where('status', 'private');
        dd($data);
    }
}
