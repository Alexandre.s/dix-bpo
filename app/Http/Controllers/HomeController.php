<?php

namespace App\Http\Controllers;
use App\Models\Noticia;
use App\Models\User;

use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
   
    public function index()
    {
        $name = auth()->user()->name;
        dd($name);
        $posts = Noticia::where('author', $name)->orderBy('id', 'DESC')->paginate(8);
        $users = User::all();
        return view('dashboard')
              ->with('posts', $posts)
              ->with('users', $users);
    }

    public function usersCount(){
        $users = DB::table('users')->count();
        $noticias = DB::table('noticias')->count();

        return view('dashboard', compact('users', 'noticias'));
    }

}
