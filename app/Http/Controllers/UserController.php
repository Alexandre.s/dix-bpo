<?php

namespace App\Http\Controllers;

use App\Models\User;

use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
Use \Carbon\Carbon;
use Illuminate\Support\Facades\DB;


class UserController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param  \App\Models\User  $model
     * @return \Illuminate\View\View
     */
    public function index(User $model)
    {
        return view('users.index', ['users' => $model->paginate(15)]);
    }

    public function formcreate(){
        return view('users.create');
    }

    public function create(Request $request){

        $name = $request->input('name');
        $email = $request->input('email');
        $password = $request->input('password');
        $password_confirmation = $request->input('password_confirmation');
        $date = Carbon::now()->toDateTimeString();
        
        if(User::all() === $email){
            return back()->withStatus(__('Email já existe no banco de dados'));
        }
        
        else{
            $password = bcrypt($password);
            DB::insert('insert into users (name, email, password, created_at, updated_at) values (?, ?, ?, ?, ?)', [$name, $email, $password, $date, $date]);
            return back()->withStatus(__('Noticia adicionada com sucesso'));
        }

    }

    public function edit($id){
        $users = User::find($id);
        return view('users.edit', compact('users'));
    }


        public function update(Request $request, $id)
        {
            $users = User::find($id);
            $users->update($request->all());
            return back()->withStatus(__('Perfil atualizado com sucesso'));
        }

        public function password(Request $request)
        {
            $users = User::find($id);

            $users->update(['password' => Hash::make($request->get('password'))]);
    
            return back()->withPasswordStatus(__('Password successfully updated.'));
        }
        public function destroy($id)
        {
            $users = User::find($id);
            $users->delete();
            return back()->withPasswordStatus(__('A notícia foi deletada'),compact('users'));
        }
    
}
