<?php

namespace App\Http\Controllers\admin;
use App\Models\Noticia;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
Use \Carbon\Carbon;
class NoticiasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('formulario.noticias.create');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $title = $request->input('title');
        $body = $request->input('body');
        $author = auth()->user()->name;
        $status = $request->input('status');
        $date = Carbon::now()->toDateTimeString();
        
        DB::insert('insert into noticias (title, body, author, status, created_at, updated_at) values (?, ?, ?, ?, ?, ?)', [$title, $body, $author, $status, $date, $date]);
        return back()->withStatus(__('Noticia adicionada com sucesso'));
    }

    public function usersCount(){
        $users = DB::table('users')->count();

        return view('dashboard', compact('users'));
    }






    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
     
    }

    public function showNoticias($id)
    {
        $posts = Noticia::find($id);
        return view('formulario.noticias.show', compact('posts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $posts = Noticia::find($id);
        return view('formulario.noticias.edit', compact('posts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $posts = Noticia::orderBy('id', 'DESC')->paginate(8);
        $dataForm = $request->all();

        $noticia = Noticia::find($id);
        
        $update = $noticia->update($dataForm);

        if($update)
            return back()->withStatus(__('Noticia alterada com sucesso'));

    }

    public function search(Request $request){
        $search = $request->get('search');
        $posts = Noticia::where('title', 'like', '%' .$search. '%')->paginate(6);
        return view('dashboard', compact('posts'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $posts = Noticia::find($id);
        $posts->delete();
        return back()->withPasswordStatus(__('A notícia foi deletada'),compact('posts'));
    }
}
