<?php

namespace App\Http\Controllers;
use App\Models\Noticia;

class PageController extends Controller
{
    /**
     * Display icons page
     *
     * @return \Illuminate\View\View
     */
    public function icons()
    {
        return view('pages.icons');
    }

    /**
     * Display maps page
     *
     * @return \Illuminate\View\View
     */
    public function maps()
    {
        $posts = Noticia::where('status', 'public')->orderBy('id', 'DESC')->paginate(8);
        return view('pages.maps')
              ->with('posts', $posts);
    }

    /**
     * Display tables page
     *
     * @return \Illuminate\View\View
     */
    public function tables()
    {
        return view('pages.tables');
    }

    /**
     * Display notifications page
     *
     * @return \Illuminate\View\View
     */
    public function notifications()
    {

        $name = auth()->user()->name;
        $posts = Noticia::where('author', $name)->where('status', 'private')->orderBy('id', 'DESC')->paginate(8);
        return view('pages.notifications')
              ->with('posts', $posts);
    }

    /**
     * Display rtl page
     *
     * @return \Illuminate\View\View
     */
    public function rtl()
    {
        return view('pages.rtl');
    }

    /**
     * Display typography page
     *
     * @return \Illuminate\View\View
     */
    public function typography()
    {
        return view('pages.typography');
    }

    /**
     * Display upgrade page
     *
     * @return \Illuminate\View\View
     */
    public function upgrade()
    {
        return view('pages.upgrade');
    }
}
